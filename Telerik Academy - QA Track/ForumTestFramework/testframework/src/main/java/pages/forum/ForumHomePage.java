package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class ForumHomePage extends BasePage {

    public ForumHomePage(WebDriver driver) {
        super(driver, "forum.homePage");
    }


    public void clickLogInButton() {
        actions.clickElement("forum.homePage.logInButton");
    }

    public void waitForElementTelerikLogo() {
        actions.waitForElementVisibleUntilTimeout("forum.homePage.telerikLogo", 10, 2);
    }

    public void assertTelerikLogoVisible() {
        actions.assertElementPresent("forum.homePage.telerikLogo");
    }

}
