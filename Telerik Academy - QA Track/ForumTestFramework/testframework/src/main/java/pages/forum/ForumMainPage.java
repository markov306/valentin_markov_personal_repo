package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class ForumMainPage extends BasePage {
    public ForumMainPage(WebDriver driver){
        super(driver, "forum.mainPage");
    }
    public void assertUserAvatarVisible() {
        actions.assertElementPresent("forum.mainPage.userAvatar");
    }

    public void waitForElementVisibleUntilTimeout() {
        actions.waitForElementVisibleUntilTimeout("forum.mainPage.userAvatar", 10, 2);
    }

}
