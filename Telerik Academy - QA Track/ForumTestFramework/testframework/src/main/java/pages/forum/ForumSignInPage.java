package pages.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class ForumSignInPage extends BasePage {
    public ForumSignInPage(WebDriver driver) {
        super(driver, "forum.signInPage");
    }

    public void enterEmail(String email) {
        actions.typeValueInField(email, "forum.signInPage.emailField");
    }

    public void enterPassword(String password) {
        actions.typeValueInField(password, "forum.signInPage.passwordField");
    }

    public void clickSignInButton() {
        actions.clickElement("forum.signInPage.signInButton");
    }


}

