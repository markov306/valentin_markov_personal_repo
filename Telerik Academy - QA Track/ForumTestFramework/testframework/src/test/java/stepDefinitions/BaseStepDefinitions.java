package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import pages.forum.ForumHomePage;
import pages.forum.ForumSignInPage;

public class BaseStepDefinitions {

    UserActions actions = new UserActions();
    ForumHomePage home = new ForumHomePage(actions.getDriver());
    ForumSignInPage signInPage = new ForumSignInPage(actions.getDriver());


    @BeforeStory
    public void setUp() {
        UserActions.loadBrowser("forum.homePage");
    }

    @AfterStory
    public static void tearDown() {
        UserActions.quitDriver();
    }
}
