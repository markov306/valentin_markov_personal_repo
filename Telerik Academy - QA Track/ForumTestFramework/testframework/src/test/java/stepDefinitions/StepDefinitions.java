package stepDefinitions;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class StepDefinitions extends BaseStepDefinitions {

    @Given("Telerik logo is visible")
    public void telerikLogoVisible() {
        home.navigateToPage();
        Assert.assertTrue(actions.isElementVisible("forum.homePage.telerikLogo"));
    }

    @When("I click on Log In button")
    public void clickOnLogInButton() {
        home.clickLogInButton();
    }

    @When("I type $email in email field")
    public void enterEmail(String email) {
        signInPage.enterEmail(email);
    }

    @When("I type $password in password field")
    public void enterPassword(String password) {
        signInPage.enterEmail(password);
    }

    @When("I click on Sign In button")
    public void clickOnSignInButton() {
        signInPage.clickSignInButton();
    }

    @Then("I am log in forum with my credentials")
    public void assertUserAvatarVisible() {
        Assert.assertTrue(actions.isElementVisible("forum.mainPage.userAvatar"));

    }

}


