package testCases.telerikForum;

import org.junit.Test;
import pages.forum.ForumHomePage;
import pages.forum.ForumMainPage;
import pages.forum.ForumSignInPage;

public class ForumTests extends BaseTest {


    @Test
    public void navigateToForum() {
        ForumHomePage home = new ForumHomePage(actions.getDriver());

        //Act
        home.navigateToPage();
        home.waitForElementTelerikLogo();

        //Assert
        home.assertTelerikLogoVisible();
    }


    @Test
    public void existingUserAuthenticated_when_validCredentialsSuppliedInLogInForm() {
        //Arrange
        navigateToForum();

        ForumSignInPage signInPage = new ForumSignInPage(actions.getDriver());
        ForumMainPage mainPage = new ForumMainPage(actions.getDriver());
        String email = "pencho@bogdanovi.com";
        String password = "Pen40NeCheti";


        //Act
        actions.clickElement("forum.homePage.logInButton");

        signInPage.enterEmail(email);
        signInPage.enterPassword(password);
        signInPage.clickSignInButton();

        //Assert
        mainPage.waitForElementVisibleUntilTimeout();
        mainPage.assertUserAvatarVisible();
    }

}


