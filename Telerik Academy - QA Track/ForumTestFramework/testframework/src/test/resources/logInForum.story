Meta:
@logInForum

Narrative:
I am registered forum user with email and password
I want to navigate to forum and log in with my email and password


Scenario: Navigate to Telerik forum
Given Telerik logo is visible
When I click on Log In button
When I type pencho@bogdanovi.com in email field
When I type Pen40NeCheti in password field
When I click on Sign In button
Then I am log in forum with my credentials