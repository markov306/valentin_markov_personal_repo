package com.telerikacademy;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BoardItem {
    String title;
    LocalDateTime dueDate;
    private Status status = Status.OPEN;

    private static Status[] statuses = Status.values();

    public BoardItem(String title, LocalDateTime dueDate) {
        setTitle(title);
        setDueDate(dueDate);
    }

    public void setTitle(String title) {
        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException();
        }
        this.title = title;
    }

    public void setDueDate(LocalDateTime dueDate) {
        if (LocalDateTime.now().isAfter(dueDate)) {
            throw new IllegalArgumentException();
        }
        this.dueDate = dueDate;
    }

    public static String formatDate(LocalDateTime dueDate) {
        DateTimeFormatter formatDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dueDate.format(formatDateTime);
    }

    public void revertStatus() {
        status = statuses[(status.ordinal() - 1)];
    }

    public void advanceStatus() {
        status = statuses[(status.ordinal() + 1)];
    }

    public String viewInfo() {
        return "'" + title + "', " + "[" + status.toString() + " | " + formatDate(dueDate) + "]";
    }

}
