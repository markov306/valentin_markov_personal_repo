package com.telerikacademy;

public enum Status {
    OPEN,
    TO_DO,
    IN_PROGRESS,
    DONE,
    VERIFIED;

    public String toString() {
        switch (this) {
            case OPEN:
                return "Open";
            case TO_DO:
                return "To Do";
            case IN_PROGRESS:
                return "In Progress";
            case DONE:
                return "Done";
            case VERIFIED:
                return "Verified";
            default:
                throw new IllegalArgumentException();
        }
    }
}
