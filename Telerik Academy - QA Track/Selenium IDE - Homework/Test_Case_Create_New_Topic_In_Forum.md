**Test case covering the following functionality of a forum:** Creation of new topic.

**Test title:** Create a new topic.

**Narative:** 
1) I want to create a new topic in forum from my windows PC. I use Google Chrome browser and i have registration in https://stage-forum.telerikacademy.com/. 
2) Before starting the test please Log out from your registration in forum. This is necessary so that you do not have to delete the whole story from yours Google Chrome browser.
3) The topic have short title and body from one or two sentence.

**Steps to reproduce:**
1. Start Google Chrome browser.
2. Go to https://www.google.com
3. Go to https://stage-forum.telerikacademy.com/
4. Click "Log in" button.
5. You log in automatically with your credential.
6. Clik on "New topic" button
7. Click to enter the title of the topic.
8. Click in the field to enter the content of the topic.
9. Create a new topic using the "Create topic" button.
10. View the new created topic.

**Expected result:**Create a new topic in forum.

**Selenium IDE Homework by Valentin Markov.**

__________________________________________________________________________________________________________
