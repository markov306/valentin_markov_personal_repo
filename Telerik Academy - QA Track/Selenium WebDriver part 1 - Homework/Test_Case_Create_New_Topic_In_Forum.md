**Test case covering the following functionality of a forum:** Creation of new topic.

**Test title:** Create a new topic.

**Narative:** 
1) I want to create a new topic in forum from my windows PC. I use Google Chrome browser and i have registration in https://stage-forum.telerikacademy.com/. 
2) The topic have short title and body from one or two sentence.

**Steps to reproduce:**

1. Start Google Chrome browser.
2. Go to https://stage-forum.telerikacademy.com/
3. Click "Log in" button.
4. Type your email - pencho@bogdanovi.com
5. Type your password - Pen40NeCheti
6. Click on "Sign in" button.
7. Clik on "New topic" button
8. Click to enter the title of the topic.
9. Click in the field to enter the content of the topic.
10. Create a new topic using the "Create topic" button.
11. View the new created topic.

**Expected result:**
Create a new topic in forum.

__________________________________________________________________________________________________________
