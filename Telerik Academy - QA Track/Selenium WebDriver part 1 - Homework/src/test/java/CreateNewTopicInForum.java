import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CreateNewTopicInForum {

    @Test
    public void CreateNewTopic() {
        //Set manually your chromedriver directory.
        System.setProperty("webdriver.chrome.driver", "C:\\Chrome and gecko driver\\chromedriver.exe");
        WebDriver webdriver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        webdriver.manage().window().maximize();

        //Arrange
        webdriver.get("https://stage-forum.telerikacademy.com");

        //Act
        //Click on "Log in" button.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("d-button-label")));
        WebElement logInButton = webdriver.findElement(By.className("d-button-label"));
        logInButton.click();

            /*Assertion for forum login page title.*/
        String ActualLogInPageTitle = webdriver.getTitle();
        String ExpectedLogInPageTitle = "Sign in";
        Assert.assertEquals(ExpectedLogInPageTitle, ActualLogInPageTitle);

        //Type email in field.
        WebElement emailField = webdriver.findElement(By.xpath("//input[@placeholder='Email']"));
        emailField.sendKeys("pencho@bogdanovi.com");

        //Type password in field.
        WebElement passWordField = webdriver.findElement(By.xpath("//input[@placeholder='Password']"));
        passWordField.sendKeys("Pen40NeCheti");

        //Click on "Sign in" button.
        WebElement signInButton = webdriver.findElement(By.id("next"));
        signInButton.click();

            /*Assertion for forum main page title.*/
        String ActualMainPageTitle = webdriver.getTitle();
        String ExpectedMainPageTitle = "Telerik Academy Forum";
        Assert.assertEquals(ExpectedMainPageTitle, ActualMainPageTitle);

        //Click on "New topic" button
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        WebElement newTopicButton = webdriver.findElement(By.id("create-topic"));
        newTopicButton.click();

        //Type topic title in field.
        WebElement topicTitleField = webdriver.findElement(By.id("reply-title"));
        topicTitleField.sendKeys("Glenfiddich 18 Year Old");

        //Type topic description in field.
        WebElement topicDescriptionField = webdriver.findElement(By.xpath("//textarea[@class='d-editor-input ember-text-area ember-view']"));
        topicDescriptionField.sendKeys("Creamy with a long, smooth and mellow finish, our 15 Year Old is the perfect example of Glenfiddich’s unique Speyside style and is widely proclaimed the best dram in the valley.");

        //Click on "Create" button
        WebElement createTopicButton = webdriver.findElement(By.xpath("//button[@class='btn btn-icon-text btn-primary create ember-view']"));
        createTopicButton.click();

        //View new topic an back to main forum page
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("title-wrapper")));
        webdriver.navigate().to("https://stage-forum.telerikacademy.com");

        //Assert
        //Check for your new topic
        boolean isDisplayed = webdriver.findElement(By.linkText("Glenfiddich 18 Year Old")).isDisplayed();
        if (isDisplayed) {
            System.out.println("Our new topic has been created successfully !!!");
        } else {
            System.out.println("The new topic has not been created");
        }

        webdriver.quit();

    }
}
