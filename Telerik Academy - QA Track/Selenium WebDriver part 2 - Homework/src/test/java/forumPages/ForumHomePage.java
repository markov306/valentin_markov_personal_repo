package forumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumHomePage {
    private WebDriver driver;

    public ForumHomePage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getLogInButton() {
        return driver.findElement(By.xpath("//span[@class='d-button-label']"));
    }

    public WebElement getAlphaPreparation() {
        return driver.findElement(By.xpath("//span[@class='category-name' and text()='Alpha Preparation']"));
    }

    public WebElement getTelerikLogo() {
        return driver.findElement(By.xpath("//img[@id='site-logo']"));
    }

    //Actions with maps
    public void logInForum() {
        getLogInButton().click();
    }

    public void navigateAlphaPrep() {
        getAlphaPreparation().click();
    }
}
