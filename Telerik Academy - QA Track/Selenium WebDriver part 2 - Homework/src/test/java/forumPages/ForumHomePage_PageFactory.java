package forumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForumHomePage_PageFactory {
    private WebDriver driver;

    public ForumHomePage_PageFactory(WebDriver webDriver) {
        driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath="//button//span[text()='Log In']")
    public  WebElement logInButton;

    @FindBy(xpath="//span[@class='category-name' and text()='Alpha Preparation']")
    public  WebElement alphaPreparation;

    @FindBy(xpath="//img[@id='site-logo']")
    public  WebElement telerikLogo;



    //Actions with maps
    public void logInForum() {
        logInButton.click();
    }

    public void navigateAlphaPrep() {
        alphaPreparation.click();
    }

    public void navigateTelerikLogo(){
        telerikLogo.click();
    }


}
