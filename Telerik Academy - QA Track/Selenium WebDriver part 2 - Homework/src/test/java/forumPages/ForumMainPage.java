package forumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class ForumMainPage {

    private WebDriver driver;

    public ForumMainPage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getNewTopicButton() {
        return driver.findElement(By.xpath("//button[@id='create-topic']"));
    }

    public WebElement getNewTopicTitle() {
        return driver.findElement(By.xpath("//input[@id='reply-title']"));
    }

    public WebElement getNewTopicContent() {
        return driver.findElement(By.xpath("//textarea[@class='d-editor-input ember-text-area ember-view']"));
    }

    public WebElement getHyperlink(){
        return driver.findElement(By.xpath("//button[@class='link btn no-text btn-icon ember-view']"));
    }

    public WebElement getHyperlinkField(){
        return driver.findElement(By.xpath("//input[@class='link-url ember-text-field ember-view']"));
    }

    public WebElement getOkHyperlinkField(){
        return driver.findElement(By.xpath("//button[@class='btn-primary btn btn-text ember-view']/span"));
    }

    public WebElement getCreateNewTopicButton() {
        return driver.findElement(By.xpath("//button[@title='Or press Ctrl+Enter']"));
    }


    //Actions with maps
    public void clickNewTopicButton(){
        getNewTopicButton().click();
    }

    public void enterNewTopicTitle(String title){
        getNewTopicTitle().sendKeys(title);
    }

    public void enterNewTopicContent(String content){
        getNewTopicContent().sendKeys(content);
    }

    public void clickToInsertLinkInTopicContent(){
        getHyperlink().click();
    }

    public void enterHyperlink(String url){
        getHyperlinkField().sendKeys(url);
    }

    public void clickToOkInsertHyperlink(){
        getOkHyperlinkField().click();
    }

    public void clickCreateNewTopicButton(){
        getCreateNewTopicButton().click();
    }

    }





