package forumPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ForumSignInPage {
    private WebDriver driver;

    public ForumSignInPage(WebDriver webDriver) {
        driver = webDriver;
    }

    public WebElement getEmailField() {
        return driver.findElement(By.xpath("//input[@placeholder='Email']"));
    }

    public WebElement getPasswordField() {
        return driver.findElement(By.xpath("//input[@placeholder='Password']"));
    }

    public WebElement getSignInButton() {
        return driver.findElement(By.xpath("//button[@id='next']"));
    }

    //Actions with maps
    public void getEnterEmail(String email) {
        getEmailField().sendKeys(email);
    }

    public void getEnterPassword(String password) {
        getPasswordField().sendKeys(password);
    }

    public void clickSignInButton() {
        getSignInButton().click();
    }


}

