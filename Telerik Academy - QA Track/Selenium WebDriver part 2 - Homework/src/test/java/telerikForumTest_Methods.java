import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class telerikForumTest_Methods {

    private WebDriver webdriver;

    @BeforeClass
    public static void classSetup() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        webdriver = new ChromeDriver(options);
        webdriver.get("https://stage-forum.telerikacademy.com");
        webdriver.manage().window().maximize();
        webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

//    @After
//    public void tearDown() {
//        webdriver.quit();
//    }


    @Test
    public void navigateToForum() {
        //Add Explicit Wait
        WebDriverWait wait = new WebDriverWait(webdriver, Duration.ofSeconds(10));

        //Act
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@id='site-logo']")));
        WebElement telerikLogo = webdriver.findElement(By.xpath("//img[@id='site-logo']"));
        WebElement learningPlatformText = webdriver.findElement(By.xpath("//li[@class='headerLink vdo learning-platform']"));

        //Assert
        assertTrue("Telerik logo isn't displayed", telerikLogo.isDisplayed());
        assertTrue("Learning platform text isn't displayed", learningPlatformText.isDisplayed());
    }

    @Test
    public void existingUserAuthenticated_when_validCredentialsSuppliedInLogInForm() {
        //Arrange
        String existingEmail = "pencho@bogdanovi.com";
        String existingPassword = "Pen40NeCheti";
        WebDriverWait wait = new WebDriverWait(webdriver, Duration.ofSeconds(10));

        //Act
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='d-button-label']")));
        WebElement logButton = webdriver.findElement(By.xpath("//span[@class='d-button-label']"));
        logButton.click();

        WebElement emailField = webdriver.findElement(By.xpath("//input[@placeholder='Email']"));
        emailField.sendKeys(existingEmail);

        WebElement passwordField = webdriver.findElement(By.xpath("//input[@placeholder='Password']"));
        passwordField.sendKeys(existingPassword);

        //#Assert for verifications to check current state in the test
        WebElement signInButton = webdriver.findElement(By.xpath("//button[@id='next']"));
        assertTrue("Sign in button isn't displayed", signInButton.isDisplayed());

        signInButton.click();

        //Assert
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@title='Pencho']")));
        WebElement userAvatar = webdriver.findElement(By.xpath("//img[@title='Pencho']"));
        assertTrue("User isn't log in and avatar isn't displayed", userAvatar.isDisplayed());

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='create-topic']")));
        WebElement newTopicButton = webdriver.findElement(By.xpath("//button[@id='create-topic']"));
        assertTrue("New topic button isn't displayed", newTopicButton.isDisplayed());
    }

    @Test
    public void existingUserCreateNewTopic() {
        //Arrange
        String existingEmail = "pencho@bogdanovi.com";
        String existingPassword = "Pen40NeCheti";
        String topicTitle = "Ford Mustang 1988";
        String topicContent = "The Ford Mustang 1988 is a series of American automobiles manufactured by Ford. In continuous production since 1964, the Mustang is currently the longest-produced Ford car nameplate. More info: ";

        WebDriverWait wait = new WebDriverWait(webdriver, Duration.ofSeconds(10));

        //Act
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']")));
        WebElement logButton = webdriver.findElement(By.xpath("//button[@class='widget-button btn btn-primary btn-small login-button btn-icon-text']"));
        logButton.click();

        WebElement emailField = webdriver.findElement(By.xpath("//input[@placeholder='Email']"));
        emailField.sendKeys(existingEmail);

        WebElement passwordField = webdriver.findElement(By.xpath("//input[@placeholder='Password']"));
        passwordField.sendKeys(existingPassword);

        WebElement signInButton = webdriver.findElement(By.xpath("//button[@id='next']"));
        signInButton.click();

        //Assert#1 for verifications current state in the test -> check if the user is logged in.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@title='Pencho']")));
        WebElement userAvatar = webdriver.findElement(By.xpath("//a[@title='Pencho']"));
        assertTrue("User avatar isn't displayed", userAvatar.isDisplayed());

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='create-topic']")));
        WebElement newTopicButton = webdriver.findElement(By.xpath("//button[@id='create-topic']"));
        newTopicButton.click();

        //Assert#2 for verifications current state in the test -> check is new topic menu is showed.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='reply-title']")));
        WebElement topicTitleField = webdriver.findElement(By.xpath("//input[@id='reply-title']"));
        assertTrue("New topic title field isn't displayed", topicTitleField.isDisplayed());

        topicTitleField.sendKeys(topicTitle);

        WebElement topicContentField = webdriver.findElement(By.xpath("//textarea[@class='d-editor-input ember-text-area ember-view']"));
        topicContentField.sendKeys(topicContent);

        WebElement topicHyperlinkButton = webdriver.findElement(By.xpath("//button[@class='link btn no-text btn-icon ember-view']"));
        topicHyperlinkButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='link-url ember-text-field ember-view']")));
        WebElement hyperlinkField = webdriver.findElement(By.xpath("//input[@class='link-url ember-text-field ember-view']"));
        hyperlinkField.sendKeys("https://en.wikipedia.org/wiki/Ford_Mustang");

        WebElement okHyperlinkButton = webdriver.findElement(By.xpath("//button[@class='btn-primary btn btn-text ember-view']/span"));
        okHyperlinkButton.click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@title='Or press Ctrl+Enter']")));
        WebElement createTopicButton = webdriver.findElement(By.xpath("//button[@title='Or press Ctrl+Enter']"));
        createTopicButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]")));
        webdriver.get("https://stage-forum.telerikacademy.com");

        //Assert for New Topic creation
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]")));
        WebElement newTopicTitle = webdriver.findElement(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]"));
        assertTrue("New topic isn't displayed", newTopicTitle.isDisplayed());

        //Log out user
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Pencho']")));

        try {
            WebElement userAvatar1 = webdriver.findElement(By.xpath("//a[@title='Pencho']"));
            userAvatar1.click();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            WebElement userAvatar1 = webdriver.findElement(By.xpath("//a[@title='Pencho']"));
            userAvatar1.click();
        }

        try {
            WebElement subMenuUserIcon = webdriver.findElement(By.xpath("//button[@class='widget-button btn-flat user-preferences-link no-text btn-icon']"));
            subMenuUserIcon.click();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            WebElement subMenuUserIcon = webdriver.findElement(By.xpath("//button[@class='widget-button btn-flat user-preferences-link no-text btn-icon']"));
            subMenuUserIcon.click();
        }

        try {
            WebElement logOutIcon = webdriver.findElement(By.xpath("//button[@class='widget-button btn-flat  btn-icon-text']"));
            logOutIcon.click();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            WebElement logOutIcon = webdriver.findElement(By.xpath("//button[@class='widget-button btn-flat  btn-icon-text']"));
            logOutIcon.click();
        }

    }
}
