import forumPages.ForumHomePage;
import forumPages.ForumHomePage_PageFactory;
import forumPages.ForumMainPage;
import forumPages.ForumSignInPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class telerikForumTest_PageFactory {

    private WebDriver webdriver;

    @BeforeClass
    public static void classSetup() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);
        webdriver = new ChromeDriver(options);
        webdriver.get("https://stage-forum.telerikacademy.com");
        webdriver.manage().window().maximize();
        webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

//    @After
//    public void tearDown() {
//        webdriver.quit();
//    }


    @Test
    public void checkSignInPageVisibility() {
        //Act
        ForumHomePage homePage = new ForumHomePage(webdriver);
        homePage.logInForum();

        //Assert
        WebElement signInButton = webdriver.findElement(By.xpath("//button[@id='next']"));
        assertTrue("Sign in button isn't displayed", signInButton.isDisplayed());
    }

    @Test
    public void checkSignInPageVisibility_PageFactory() {
        //Act
        ForumHomePage_PageFactory homePage_pageFactory = new ForumHomePage_PageFactory(webdriver);
       homePage_pageFactory.logInForum();
//        homePage_pageFactory.navigateAlphaPrep();
//        homePage_pageFactory.navigateTelerikLogo();

        //Assert
        WebElement emailField = webdriver.findElement(By.xpath("//input[@placeholder='Email']"));
        assertTrue("Email field isn't displayed", emailField.isDisplayed());

        WebElement passwordField = webdriver.findElement(By.xpath("//input[@placeholder='Password']"));
        assertTrue("Password field isn't displayed", passwordField.isDisplayed());
    }


    @Test
    public void existingUserAuthenticated_when_validCredentialsSuppliedInLogInForm() {
        //Arrange
        String existingEmail = "pencho@bogdanovi.com";
        String existingPassword = "Pen40NeCheti";
        WebDriverWait wait = new WebDriverWait(webdriver, Duration.ofSeconds(10));
        ForumHomePage homePage = new ForumHomePage(webdriver);
        homePage.logInForum();

        //Act
        ForumSignInPage signInPage = new ForumSignInPage(webdriver);

        signInPage.getEnterEmail(existingEmail);
        signInPage.getEnterPassword(existingPassword);
        signInPage.clickSignInButton();

        //Assert
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@title='Pencho']")));
        WebElement userAvatar = webdriver.findElement(By.xpath("//img[@title='Pencho']"));
        assertTrue("User avatar isn't displayed", userAvatar.isDisplayed());

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='create-topic']")));
        WebElement newTopicButton = webdriver.findElement(By.xpath("//button[@id='create-topic']"));
        assertTrue("New topic button isn't displayed", newTopicButton.isDisplayed());
    }

    @Test
    public void existingUserCreateNewTopicWithLink() {
        //Arrange
        String existingEmail = "pencho@bogdanovi.com";
        String existingPassword = "Pen40NeCheti";
        String topicTitle = "Dodge Charger 1944";
        String topicContent = "The Dodge Charger 1944 is a model of automobile marketed by Dodge in various forms over seven generations between 1966 and today. More info: ";

        WebDriverWait wait = new WebDriverWait(webdriver, Duration.ofSeconds(10));
        ForumHomePage homePage = new ForumHomePage(webdriver);
        homePage.logInForum();

        //Act
        ForumSignInPage signInPage = new ForumSignInPage(webdriver);
        signInPage.getEnterEmail(existingEmail);
        signInPage.getEnterPassword(existingPassword);
        signInPage.clickSignInButton();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='create-topic']")));
        ForumMainPage mainPage = new ForumMainPage(webdriver);

        mainPage.clickNewTopicButton();
        mainPage.enterNewTopicTitle(topicTitle);
        mainPage.enterNewTopicContent(topicContent);

        mainPage.clickToInsertLinkInTopicContent();
        mainPage.enterHyperlink("https://en.wikipedia.org/wiki/Dodge_Charger");
        mainPage.clickToOkInsertHyperlink();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@title='Or press Ctrl+Enter']")));
        mainPage.clickCreateNewTopicButton();

        //Assert
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]")));
        webdriver.get("https://stage-forum.telerikacademy.com");

        //Assert for New Topic creation
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]")));
        WebElement newTopicTitle = webdriver.findElement(By.xpath("//a[text()[contains(.,'" + topicTitle + "')]]"));
        assertTrue("New topic isn't displayed", newTopicTitle.isDisplayed());
    }
}
