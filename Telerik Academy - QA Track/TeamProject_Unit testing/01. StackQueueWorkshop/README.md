# Stack

###### A stack is a *LIFO* (last in first out) data structure

#### Real life stacks

- A stack of dishes
- A pile of t-shirts

#### Some practical use-cases of a stack:

- Reversing order
- "Undo" functionality
- Several important algorithms (like DFS)
- The call stack - keeping track of currently active methods

#### Common operations on a stack

- push(element) - add an element to the stack
- pop() - remove an element from the top of the stack
- peek() - get the value of the first element, without removing it


# Queue

###### A queue is a *FIFO* (first in first out) data structure

#### Some practical use-cases of a queue:

- Breadth-First Search in a tree/graph
- In a call center - to manage the people who need to be helped
- Printing documents - the printer can only print one at a time, the others are next in line (First in, first out)

#### Common operations on a queue

- *enqueue(element)* / *offer(element)* - adding an element to the queue
- *dequeue()* / *remove()*  - removing an element from the queue
- *peek()* / *element()* - get the value of the first element, without removing it

### Queue vs Stack
<br>

![picture](images/queueVSstack.png)

### Your task is to write unit tests for the given implementations of a stack and a queue.

#### The task:

1. Each team should aim for 90%+ coverage.
1. Divide the number of tests per team member equally.
1. Use common naming convention.
1. Name your commits properly.

##### Tasks for exercise - stack and queue (optional):

 - [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)
 - [Next Greater Element](https://leetcode.com/problems/next-greater-element-i/)
 - [Implement Queue Using Stack](https://leetcode.com/problems/implement-queue-using-stacks/description/)

##### Additional tasks (optional):

 - [Backspace String Compare](https://leetcode.com/problems/backspace-string-compare/)
 - [Lemonade Change](https://leetcode.com/problems/lemonade-change/)
 - [Baseball Game](https://leetcode.com/problems/baseball-game/)
 - [Asteroid Collision](https://leetcode.com/problems/asteroid-collision/)
