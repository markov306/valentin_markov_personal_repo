package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {
    
    private List<T> elements;
    
    public StackImpl() {
        this.elements = new ArrayList<>();
    }
    
    public StackImpl(Iterable<T> elements) {
        this.elements = new ArrayList<>();
        for (T element : elements) {
            this.elements.add(element);
        }
    }
    
    @Override
    public void push(T element) {
        elements.add(element);
    }
    
    @Override
    public T pop() {
        if (size() == 0) {
            throw new IllegalArgumentException();
        }
        return elements.remove(size() - 1);
    }
    
    @Override
    public T peek() {
        if (size() == 0) {
            throw new IllegalArgumentException();
        }
        return elements.get(size() - 1);
    }
    
    @Override
    public int size() {
        return elements.size();
    }
    
    @Override
    public boolean isEmpty() {
        return !(size() > 0);
    }
    
}
