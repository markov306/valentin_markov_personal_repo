import com.telerikacademy.StackImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class StackTests {

    private StackImpl<Integer> testStack;

    @BeforeEach
    public void before() {
        this.testStack = new StackImpl<>();
    }

    @Test
    public void should_Crate_Constructor_When_Have_Elements() {
        ArrayList<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(1);
        test.add(1);

        StackImpl<Integer> testStackNew = new StackImpl<>(test);

        Assertions.assertEquals(test.size(), testStackNew.size());

    }

    @Test
    public void should_Store_Elements_When_Multiply_Elements() {
        StackImpl<Integer> testStackNew = new StackImpl<>();

        testStack.push(2);
        Assertions.assertEquals(1, testStackNew.size());
    }

}
