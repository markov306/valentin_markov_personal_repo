| Test Case Title | Functionality | Priority| Technique |
| ------ | ------ |------ | ------ |
| Create a new topic from android/windows/ios devices. | topic creation |Prio 1 | Pairwise testing |
| The topic becomes inaccessible after it is deleted. | topic creation |Prio 3 | State transition testing  |
| Create a topic using Latin letters / Cyrillic letters. | topic creation |Prio 2 | Use case testing |
| Number of comments received in one topic. | topic creation | Prio 2 | BVA |
| Attach files larger than X mb to a created  topic. | topic creation | Prio 1 | BVA |
| Create a topic with a duplicate title but with different content and attachments. | topic creation | Prio 3 | Classification trees | 
| Edit my comment from android/windows/ios devices. | commenting | Prio 1 | Pairwise testing | 
| Create a blank comment | commenting | Prio 3 | Equivalence partitioning | 
| Edit another user's comment | commenting | Prio 1 | Use case testing | 
| Attach files larger than X mb to a created  comment | commenting | Prio 2 | BVA |
| Create X comments in a specific topic | commenting | Prio 3 | BVA |
| The user receives a notification about his registration in the forum | notifications | Prio 1 | Equivalence partitioning | 
| The user receives a notification about a new topic in the forum | notifications | Prio 2 | Decision tables | 
| The user receives a notification for a new comment in a topic created by him | notifications | Prio 2 | Decision tables | 
| The user can receive X notifications for comments and topics | notifications | Prio 3 | BVA |
| The notification for a new comment disappears after the user views it. | notifications | Prio 1 | State transition testing | 
