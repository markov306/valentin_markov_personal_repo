package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        this.products = new ArrayList<>();
    }

    public void setName(String name) {
        if (name.length() < 2 || name.length() > 15) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        checkForNull(product);
        products.add(product);
    }

    public void removeProduct(Product product) {
        checkForNull(product);
        if(!products.contains(product)) {
            throw new IllegalArgumentException();
        }
        products.remove(product);
    }

    private void checkForNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }
    }


    public String print() {
        StringBuilder sb = new StringBuilder();

        for (Product i : products) {
            sb.append(i.print()).append(System.lineSeparator());
        }

        return " #Category: " + name +
                System.lineSeparator()
                + sb;

    }
}
    

