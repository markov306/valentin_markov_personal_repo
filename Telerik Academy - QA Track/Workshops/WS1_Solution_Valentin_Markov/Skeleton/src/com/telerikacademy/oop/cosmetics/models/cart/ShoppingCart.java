package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        this.productList = new ArrayList<>();
    }
    
    public List<Product> getProductList() {
        return this.productList;
    }
    
    public void addProduct(Product product) {
        checkForNull(product);
        productList.add(product);
    }
    
    public void removeProduct(Product product) {
        checkForNull(product);
        if(!productList.contains(product)) {
            throw new IllegalArgumentException();
        }
        productList.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        checkForNull(product);
        for(Product p : productList) {
            if(p.getName().equals(product.getName())) {
                return true;
            }
        }
        return false;
    }
    private void checkForNull(Product product) {
        if (product == null){
            throw new IllegalArgumentException();
        }
    }


    public double totalPrice() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
