package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(double price, String name, String brand, GenderType gender) {
        this.price = price;
        this.name = name;
        this.brand = brand;
        this.gender = gender;
    }

    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return this.name;
    }

    public String getBrand() {
        return brand;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setName(String name) {
        if (name.length() < 3 || name.length() > 10) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public void setBrand(String brand) {
        if (brand.length() < 2 || brand.length() > 10) {
            throw new IllegalArgumentException();
        }
        this.brand = brand;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }


    public String print() {
        return " #" + name + " " + brand +
                System.lineSeparator() +
                " #Price: " + price +
                System.lineSeparator() +
                " #Gender: " + gender +
                System.lineSeparator() +
                "===";
    }

}
