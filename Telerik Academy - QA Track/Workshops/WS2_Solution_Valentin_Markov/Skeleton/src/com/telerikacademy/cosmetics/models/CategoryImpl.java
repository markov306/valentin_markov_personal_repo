package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //use constants for validations values
    public static final int NAME_MIN_SIZE = 2;
    public static final int NAME_MAX_SIZE = 15;
    private static final String NAME_NULL = "The name must not be empty";
    private static final String INVALID_NAME_SIZE_MSG = String.format("Name should be between %d and %d symbols.",
            NAME_MIN_SIZE, NAME_MAX_SIZE);
    private static final String PRODUCT_NULL = "Product cannot be null!";
    private static final String ERROR_DELETE = "The product has been deleted";




    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        setName(name);
        products = new ArrayList<>();


    }
    public void setName(String name) {
        if (name == null){
            throw new IllegalArgumentException(NAME_NULL);
        }
        if (name.length() < NAME_MIN_SIZE || name.length()> NAME_MAX_SIZE) {
            throw new IllegalArgumentException(INVALID_NAME_SIZE_MSG);
        }
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_NULL);
        }
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_NULL);
        }
        if (!products.contains(product)) {
            throw new IllegalArgumentException(ERROR_DELETE);
        }
        products.remove(product);
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s%n" +
                    " #No product in this category", name);
        }
        //finish ProductBase class before implementing this method
        StringBuilder result = new StringBuilder();
        for(Product product: products){
            result.append(product.print());
        }
        return String.format("#Category: %s%n%s", name, result.toString());
        //throw new NotImplementedException();
    }
    
}
