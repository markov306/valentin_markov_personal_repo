package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;


public class CreamImpl extends ProductBase implements Cream {

    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    public void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    public ScentType getScent() {
        return scent;

    }
        @Override
        public String print(){
            return String.format("%s%n #Scent: %s%n ===", super.print(), scent );
    }


}
