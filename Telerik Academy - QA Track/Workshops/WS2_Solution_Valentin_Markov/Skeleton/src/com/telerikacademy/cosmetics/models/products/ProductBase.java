package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductBase implements Product {

    private String name;
    private String brand;
    private double price;
    private GenderType gender;


    private static final int NAME_MIN_SIZE = 3;
    private static final int MAX_SIZE = 10;
    private static final int BRAND_MIN_SIZE = 2;
    private static final String INVALID_NAME_SIZE_MESSAGE = String.format("Name should be between %d and %d symbols.",
            NAME_MIN_SIZE, MAX_SIZE);
    private static final String INVALID_BRAND_SIZE_MESSAGE = String.format("Name should be between %d and %d symbols.",
            BRAND_MIN_SIZE, MAX_SIZE);


    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("The name must not be null or empty");
        }
        if (name.length() < NAME_MIN_SIZE || name.length() > MAX_SIZE) {
            throw new IllegalArgumentException(INVALID_NAME_SIZE_MESSAGE);
        }
        this.name = name;
    }

    private void setBrand(String brand) {
        if (brand == null) {
            throw new IllegalArgumentException("The brand must not be null or empty");
        }
        if (brand.length() < BRAND_MIN_SIZE || brand.length() > MAX_SIZE) {
            throw new IllegalArgumentException(INVALID_BRAND_SIZE_MESSAGE);
        }
        this.brand = brand;

    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n Price: $%.2f%n #Gender: %s", name, brand, price, gender);
    }
}
