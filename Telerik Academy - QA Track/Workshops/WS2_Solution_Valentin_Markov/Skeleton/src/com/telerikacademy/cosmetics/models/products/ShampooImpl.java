package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    private UsageType everyDay;
    
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType everyDay) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        this.everyDay = everyDay;
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return everyDay;
    }
    public void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters can't be negative number!");
        }
        this.milliliters = milliliters;
    }
        @Override
        public String print(){
            return String.format("%s%n #Milliliters: %d%n #Usage: %s%n===", super.print(), milliliters, everyDay );

        }
    }


