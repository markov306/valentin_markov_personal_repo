package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;


public class TicketImpl implements Ticket {
    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public String print() {
        return null;
    }

    @Override
    public double getAdministrativeCosts() {
        return 0;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return journey.calculateTravelCosts() * administrativeCosts;
    }

    public String toString() {
        return String.format("Ticket ----%nDestination: %s%nPrice: %.2f%n",
                journey.getDestination(), calculatePrice());
    }
}



