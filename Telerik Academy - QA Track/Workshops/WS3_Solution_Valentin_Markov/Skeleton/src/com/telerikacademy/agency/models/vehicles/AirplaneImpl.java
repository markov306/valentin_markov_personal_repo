package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;


public class AirplaneImpl extends VehicleBase implements Airplane {
    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setHasFreeFood(hasFreeFood);

    }

    public void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public String toString() {
        return String.format("%s%nPassenger capacity: %d%nPrice per kilometer: %.2f%nVehicle type: %s%n" +
                "Has free food: %b%n", super.printClassName(), getPassengerCapacity(), getPricePerKilometer(),
                getType(), hasFreeFood());
    }
}
