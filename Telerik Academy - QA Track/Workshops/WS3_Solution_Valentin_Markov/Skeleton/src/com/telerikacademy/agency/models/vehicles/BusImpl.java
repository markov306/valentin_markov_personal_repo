package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    public static final int MIN_PASSENGER_BUS = 10;
    public static final int MAX_PASSENGER_BUS = 50;
    public static final String ERROR_MESSAGE = String.format("A bus cannot have less than %s passengers or more than " +
            "%s passengers.", MIN_PASSENGER_BUS, MAX_PASSENGER_BUS);

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);

        if (passengerCapacity < MIN_PASSENGER_BUS || passengerCapacity > MAX_PASSENGER_BUS) {
            throw new IllegalArgumentException(ERROR_MESSAGE);
        }
    }

    @Override
    public String toString() {

        return String.format("%s%nPassenger capacity: %d%nPrice per kilometer: %.2f%nVehicle type: %s%n",
                super.printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType());
    }
}

