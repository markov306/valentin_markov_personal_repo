package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {
    private static final int MIN_COUNT_CARTS = 1;
    private static final int MAX_COUNT_CARTS = 15;
    private static final String ERROR_CARTS_MESSAGE = String.format("A train cannot have less than %d cart or more than %d carts.",
            MIN_COUNT_CARTS, MAX_COUNT_CARTS);
    private static final int MIN_PASSENGER = 30;
    private static final int MAX_PASSENGER = 150;
    private static final String ERROR_CAPACITY_MESSAGE = String.format("A train cannot have less than %d passengers or more than %d passengers.",
            MIN_PASSENGER, MAX_PASSENGER);
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);

        if (passengerCapacity < MIN_PASSENGER || passengerCapacity > MAX_PASSENGER) {
            throw new IllegalArgumentException(ERROR_CAPACITY_MESSAGE);
        }
        setCarts(carts);

    }

    public void setCarts(int carts) {
        if (carts < MIN_COUNT_CARTS || carts > MAX_COUNT_CARTS) {
            throw new IllegalArgumentException(ERROR_CARTS_MESSAGE);
        }
        this.carts = carts;
    }

    @Override
    public String print() {
        return null;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String toString() {
        return String.format("%s%nPassenger capacity: %d%nPrice per kilometer: %.2f%nVehicle type: %s%nCarts amount: %d%n",
                super.printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType(), getCarts());
    }
}
