package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;
    private static final String ERROR_PRICE_PER_KILOMETER_MESSAGE = String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
            MIN_PRICE_PER_KILOMETER, MAX_PRICE_PER_KILOMETER);
    private double pricePerKilometer;
    private VehicleType type;
    private int passengerCapacity;
    private static final String TYPE_CANT_BE_NULL = "Type can't be null";
    private static final int MIN_LIMIT_PASSENGERS = 1;
    private static final int MAX_LIMIT_PASSENGERS = 800;
    private static final String LIMIT_PASSENGER_MESSAGE = String.format("A vehicle with less than %s passengers or more than %s passengers cannot exist!",
            MIN_LIMIT_PASSENGERS, MAX_LIMIT_PASSENGERS);

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);

        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }

    private void setPassengerCapacity(int passengerCapacity) {

        if (passengerCapacity < MIN_LIMIT_PASSENGERS || passengerCapacity > MAX_LIMIT_PASSENGERS) {
            throw new IllegalArgumentException(LIMIT_PASSENGER_MESSAGE);
        }
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {

        if (pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER) {
            throw new IllegalArgumentException(ERROR_PRICE_PER_KILOMETER_MESSAGE);
        }
        this.pricePerKilometer = pricePerKilometer;
    }


    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public VehicleType getType() {
        return type;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public String printClassName() {
        return String.format("%s -----", getClass().getSimpleName().replace("Impl", ""));
    }

    @Override
    public String print() {
        return printClassName();
    }
}
