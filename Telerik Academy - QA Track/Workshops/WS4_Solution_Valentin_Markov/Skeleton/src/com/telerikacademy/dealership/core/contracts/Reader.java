package com.telerikacademy.dealership.core.contracts;

public interface Reader {
    
    String readLine();
    
}
