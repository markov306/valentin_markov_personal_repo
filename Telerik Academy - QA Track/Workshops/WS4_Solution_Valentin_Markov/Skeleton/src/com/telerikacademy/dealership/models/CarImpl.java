package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.ArrayList;
import java.util.List;

public class CarImpl extends VehicleBase implements Car {

    private static final String SEATS_FIELD = "Seats";
    private static int MIN_SEATS = 1;
    private static int MAX_SEATS = 10;

    //look in DealershipFactoryImpl - use it to create proper constructor
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    public void setSeats(int seats) {
        Validator.ValidateIntRange(seats, MIN_SEATS, MAX_SEATS, String.format(LENGTH_FIELD_MESSAGE_FORMAT, SEATS_FIELD, MIN_SEATS, MAX_SEATS));
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %d", SEATS_FIELD, getSeats());
    }
}
