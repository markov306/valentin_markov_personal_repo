package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.contracts.Comment;

public class CommentImpl implements Comment {
    
    private final static String COMMENT_HEADER = "    ----------";
    private final static String CONTENT_FIELD = "Content";
    private final static String COMMENT_INDENTATION = "    ";
    private final static String AUTHOR_HEADER = "      User: ";
    
    private String content;
    private String author;
    
    public CommentImpl(String content, String author) {
        
        this.content = content;
        this.author = author;
        validateState();
    }
    
    public String getContent() {
        return this.content;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s", COMMENT_HEADER)).append(System.lineSeparator());
        builder.append(COMMENT_INDENTATION).append(content).append(System.lineSeparator());
        builder.append(AUTHOR_HEADER).append(author).append(System.lineSeparator());
        builder.append(String.format("%s", COMMENT_HEADER)).append(System.lineSeparator());
        
        return builder.toString();
    }
    
    void validateState() {
        
        Validator.ValidateNull(content, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, CONTENT_FIELD));
        Validator.ValidateNull(author, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, CONTENT_FIELD));
        Validator.ValidateIntRange(content.length(), ModelsConstants.MIN_COMMENT_LENGTH, ModelsConstants.MAX_COMMENT_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        CONTENT_FIELD, ModelsConstants.MIN_COMMENT_LENGTH, ModelsConstants.MAX_COMMENT_LENGTH));
    }
    
}
