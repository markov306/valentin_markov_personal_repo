package com.telerikacademy.dealership.models;

public class ModelsConstants {
    
    // String lengths
    public final static int MIN_NAME_LENGTH = 2;
    public final static int MAX_NAME_LENGTH = 20;
    public final static int MIN_PASSWORD_LENGTH = 5;
    public final static int MAX_PASSWORD_LENGTH = 30;
    public final static int MIN_CATEGORY_LENGTH = 3;
    public final static int MAX_CATEGORY_LENGTH = 10;
    public final static int MIN_MAKE_LENGTH = 2;
    public final static int MAX_MAKE_LENGTH = 15;
    public final static int MIN_MODEL_LENGTH = 1;
    public final static int MAX_MODEL_LENGTH = 15;
    public final static int MIN_COMMENT_LENGTH = 3;
    public final static int MAX_COMMENT_LENGTH = 200;
    
    // Numbers validation
    public final static int MIN_WHEELS = 2;
    public final static int MAX_WHEELS = 10;
    public final static double MIN_PRICE = 0.0;
    public final static double MAX_PRICE = 1000000.0;
    public final static int MIN_SEATS = 1;
    public final static int MAX_SEATS = 10;
    public final static int MIN_CAPACITY = 1;
    public final static int MAX_CAPACITY = 100;
    
    // Strings for validation
    public final static String STRING_MUST_BE_BETWEEN_MIN_AND_MAX = "%s must be between %d and %d characters long!";
    public final static String NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX = "%s must be between %s and %s!";
    
    // Vehicle max to add if not VIP
    public final static int MAX_VEHICLES_TO_ADD = 5;
    
    // Username pattern
    public final static String USERNAME_PATTERN = "^[A-Za-z0-9]+$";
    public final static String PASSWORD_PATTERN = "^[A-Za-z0-9@*_-]+$";
    
    // Strings for vehicles, comments and users
    public final static String INVALID_SYMBOLS = "%s contains invalid symbols!";
    
    public final static String USER_TO_STRING = "Username: %s, FullName: %s %s, Role: %s";
    
    public final static String COMMENT_CANNOT_BE_NULL = "Comment cannot be null!";
    public final static String VEHICLE_CANNOT_BE_NULL = "Vehicle cannot be null!";
    
    public final static String NOT_AN_VIP_USER_VEHICLES_ADD = "You are not VIP and cannot add more than %d vehicles!";
    public final static String ADMIN_CANNOT_ADD_VEHICLES = "You are an admin and therefore cannot add vehicles!";
    
    public final static String YOU_ARE_NOT_THE_AUTHOR = "You are not the author!";
    public final static String USER_CANNOT_BE_NULL = "User cannot be null!";
    
    // Added additionally
    public final static String FIELD_CANNOT_BE_NULL = "%s cannot be null!";
    
}
