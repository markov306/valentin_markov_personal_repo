package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import java.util.List;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private static final String CATEGORY_FIELD = "Category";
    private static int MIN_LENGTH = 3;
    private static int MAX_LENGTH = 10;

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %s", CATEGORY_FIELD, getCategory());
    }

    public void setCategory(String category) {
        Validator.ValidateNull(category,String.format(NULL_FIELD_MESSAGE_FORMAT, CATEGORY_FIELD));
        Validator.ValidateIntRange(category.length(), MIN_LENGTH, MAX_LENGTH, String.format(CHARACTERS_LENGTH_FIELD_MESSAGE_FORMAT, CATEGORY_FIELD, MIN_LENGTH, MAX_LENGTH));
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }

}
