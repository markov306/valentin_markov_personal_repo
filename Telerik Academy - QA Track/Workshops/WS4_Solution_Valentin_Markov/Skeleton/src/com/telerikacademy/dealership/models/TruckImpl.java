package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private static final String WIGHT_CAPACITY_FIELD = "Weight Capacity";
    private static int MIN_CAPACITY = 1;
    private static int MAX_CAPACITY = 100;

    private int weightCapacity;

    //look in DealershipFactoryImpl - use it to create proper constructor
    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %dt", WIGHT_CAPACITY_FIELD, getWeightCapacity());
    }

    public void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity, MIN_CAPACITY, MAX_CAPACITY, String.format(LENGTH_FIELD_MESSAGE_FORMAT, WIGHT_CAPACITY_FIELD, MIN_CAPACITY, MAX_CAPACITY));
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

}
