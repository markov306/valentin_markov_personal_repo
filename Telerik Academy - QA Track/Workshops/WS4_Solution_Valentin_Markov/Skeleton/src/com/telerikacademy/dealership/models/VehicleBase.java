package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Commentable;
import com.telerikacademy.dealership.models.contracts.Priceable;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String TYPE_FIELD = "Type";

    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    private static final int MIN_MAKE_AND_MODEL_LENGTH = 2;
    private static final int MAX_MAKE_AND_MODEL_LENGTH = 15;
    private static final double MIN_PRICE = 0.0d;
    private static final double MAX_PRICE = 1000000.0d;
    public static final String NULL_FIELD_MESSAGE_FORMAT = "%s of vehicle cannot be NULL";
    public static final String LENGTH_FIELD_MESSAGE_FORMAT = "%s must be between %d and %d!";
    public static final String CHARACTERS_LENGTH_FIELD_MESSAGE_FORMAT = "%s must be between %d and %d characters long!";

    private static final String COMMENT = "Comment";

    //add fields
    private List<Comment> comments;
    private  String make;
    private  String model;
    private  double price;
    private  VehicleType vehicleType;

    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        comments = new ArrayList<>();
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);
    }

    public void setMake(String make) {
        Validator.ValidateNull(make, String.format(NULL_FIELD_MESSAGE_FORMAT, MAKE_FIELD));
        Validator.ValidateIntRange(make.length(), MIN_MAKE_AND_MODEL_LENGTH, MAX_MAKE_AND_MODEL_LENGTH,
                String.format(CHARACTERS_LENGTH_FIELD_MESSAGE_FORMAT, MAKE_FIELD, MIN_MAKE_AND_MODEL_LENGTH, MAX_MAKE_AND_MODEL_LENGTH));
        this.make = make;
    }

    public void setModel(String model) {
        Validator.ValidateNull(model, String.format(NULL_FIELD_MESSAGE_FORMAT, MODEL_FIELD));
        Validator.ValidateIntRange(model.length(), MIN_MAKE_AND_MODEL_LENGTH, MAX_MAKE_AND_MODEL_LENGTH, String.format(CHARACTERS_LENGTH_FIELD_MESSAGE_FORMAT, MODEL_FIELD, MIN_MAKE_AND_MODEL_LENGTH, MAX_MAKE_AND_MODEL_LENGTH));
        this.model = model;
    }

    public void setPrice(double price) {
        Validator.ValidateDecimalRange(price, MIN_PRICE, MAX_PRICE, String.format("Price must be between %.1f and %.1f!",MIN_PRICE, MAX_PRICE));
        this.price = price;
    }

    public void setVehicleType(VehicleType vehicleType) {
        Validator.ValidateNull(vehicleType, String.format(NULL_FIELD_MESSAGE_FORMAT, TYPE_FIELD));
        this.vehicleType = vehicleType;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    @Override
    public void addComment(Comment comment) {
        Validator.ValidateNull(comment, String.format(NULL_FIELD_MESSAGE_FORMAT, COMMENT));
        comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {
        Validator.ValidateNull(comment, String.format(NULL_FIELD_MESSAGE_FORMAT, COMMENT));
        comments.remove(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s", MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, getWheels())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }
    
    //needs to be overridden by child classes to include their specific info
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
    
}
